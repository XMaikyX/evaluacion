﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figuras
{
    class Rectangulo : Trigonometria
    {
        int resultado;
        private int bases;
        public int Bases
        {
            get { return bases; }
            set { bases = value; }
        }
        private int altura;

        public int Altura
        {
            get { return altura; }
            set { altura = value; }
        }
        public void Area()
        {
            resultado = bases * altura;
        }

        int Trigonometria.Bases()
        {
            // throw new NotImplementedException();
            return 20;
        }

        int Trigonometria.Altura()
        {
            // throw new NotImplementedException();
            return 14;
        }
        public int Resultado()
        {
            //throw new NotImplementedException();
            return resultado;
        }

    }
}
