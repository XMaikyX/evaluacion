﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figuras
{
    class Circulo : Trigonometria
    {
        int valor;
        private int radio;

        public int Radio
        {
            get { return radio; }
            set { radio = value; }
        }


        public void Circunferencia()
        {
            valor = radio * 2;
        }
        public int Altura()
        {
            // throw new NotImplementedException();
            return 5;
        }

        public int Bases()
        {
            // throw new NotImplementedException();
            return 1;
        }
        public int Resultado()
        {
            //throw new NotImplementedException();
            return (Bases() * Altura()) * 2;
        }
        
    }


}
