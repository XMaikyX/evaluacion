﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figuras
{
    interface Trigonometria
    {
        int Resultado();

        int Bases();

        int Altura();
    }
}
